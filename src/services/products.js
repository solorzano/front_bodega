/**
 * Created by asolorzano on 14-08-17.
 */
import backofficeServices from './front-office'
import moment from "moment";

const ProductsServices = {}


ProductsServices.findBy = (q) => {

    return backofficeServices.get('product/' + q)
        .then(res => res.data)

}

ProductsServices.findByOrdersProduct = (product_id, orders_id) => {
    //console.log(orders);return false;


    return backofficeServices.get('providers/' + product_id.id + '/' + orders_id + '/')
        .then(res => res.data)


}

ProductsServices.addOrdersProduct = (product_id, quantity, orders_id) => {
    return backofficeServices.post('providers/', {
        'product_id': product_id,
        'orders_id': orders_id,
        'quantity': quantity
    })
        .then(res => res.data)
        .catch((err) => {
            console.error(err)
        })

}

ProductsServices.updateOrderProduct = function (q) {

    //console.log(q);return false;

    return backofficeServices.put('providers/' + q.id + '/', q)
        .then(res => res)
        .catch((err) => {
            console.error(err)
        })

}

export default ProductsServices