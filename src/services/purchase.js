/**
 * Created by asolorzano on 10-08-17.
 */
import backofficeServices from './front-office'

const purchaseService = {}

/***
 * Find all purchase
 */
purchaseService.findAll = () => {

    return backofficeServices.get('purchase/')
        .then(res => res.data)
}


purchaseService.findPurchaseInDispatch = () => {
    return backofficeServices.get('purchaseDispatch/')
        .then(res => res.data)
}

/**
 * Service update status
 */
purchaseService.update = function (q) {
    return backofficeServices.put('purchase/' + q.id + '/', q)
        .then(res => res)

}

purchaseService.findBy = (q) => {
    return backofficeServices.get('purchase/' + q)
        .then(res => res.data)

}

export default purchaseService