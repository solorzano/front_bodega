/**
 * Created by asolorzano on 14-08-17.
 */
import backofficeServices from './front-office'
import moment from "moment";

const OrderServices = {}

/***
 * Find all orders
 */
OrderServices.findAll = () => {
    return backofficeServices.get('orders/')
        .then(res => res.data)
}

/**
 * Service update status
 */
OrderServices.update = (q) => {
    return backofficeServices.put('orders/' + q.id + '/', q)
        .then(res => res.data)

}

OrderServices.findOrderActive = () => {
    return backofficeServices.get('ordersFind/')
        .then(res => res.data)
}

OrderServices.findBy = (q) => {
    return backofficeServices.get('orders/' + q)
        .then(res => res.data)

}


OrderServices.create = (q) => {
    //console.log(q);return false;
    return backofficeServices.post('orders/', {
        status: 1,
        name: q
    })
        .then(response => response.data)
        .catch((err) => {
            console.error(err)
        })

}

export default OrderServices