/**
 * Created by asolorzano on 10-08-17.
 */

import trae from 'trae'
import configService from './config'

const backofficeServices = trae.create({
    baseUrl: configService.apiUrl
})
export default backofficeServices