import Vue from 'vue'
import App from './App.vue'
import Materials from "vue-materials"
import router from './router/index'
import Vuetable from 'vuetable-2';
import VueNotifications from 'vue-notifications'

import JsonExcel from 'vue-json-excel';
import DataTables from 'vue-data-tables'
import Modal from './components/reusable/modal.vue'


import miniToastr from 'mini-toastr'

// If using mini-toastr, provide additional configuration
const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
    return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
}

// Activate plugin




Vue.use(Materials)
Vue.use(Vuetable)
Vue.use(DataTables)
Vue.use(VueNotifications, options)
Vue.component('modal-component', Modal)
Vue.component('downloadExcel', JsonExcel);




new Vue({
    el: '#app',
    router,
    render: h => h(App)
})
