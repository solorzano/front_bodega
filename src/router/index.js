import Vue from 'vue'
import Router from 'vue-router'
//noinspection JSUnresolvedVariable
import Purchase from '../components/modules/purchase/index.vue'
import Orders from '../components/modules/orders/index.vue'
import Sending from '../components/modules/sending/index.vue'
import Login from '../components/modules/login/index.vue'
//import DetailView from '../components/DetailView'
//import PostView from '../components/PostView'
Vue.use(Router)
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
        },
        {
            path: '/purchase',
            name: 'purchase',
            component: Purchase,
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders,
        },
        {
            path: '/sending',
            name: 'sending',
            component: Sending,
        }
    ]
})
